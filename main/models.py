from django.db import models
from django_countries.fields import CountryField


class Film(models.Model):
    name = models.CharField('name of film', max_length=255, blank=True, null=True)
    description = models.TextField('description of film', blank=True, null=True)
    country = CountryField('country', blank_label='(select country)')
    budget = models.DecimalField('budget of film', decimal_places=2, max_digits=50, null=True, blank=True)
    release_date = models.DateField('release date', null=True, blank=True)
    genre = models.ManyToManyField('Genre', related_name='film_genre')
    access = models.ForeignKey('Access', on_delete=models.CASCADE, name='access')
    actors = models.ManyToManyField('Actor', name='actors')
    main_photo = models.ImageField('photo', upload_to='media/film_main_photo', null=True, blank=True)
    fake_views = models.IntegerField('views',null=True, blank=True)

    def __str__(self):
        return self.name


class Actor(models.Model):
    name = models.CharField('name of actor', max_length=255, null=True, blank=True)
    last_name = models.CharField('last name of actor', max_length=255, blank=True, null=True)
    films = models.ManyToManyField(Film, name='films', blank=True)
    photo = models.ImageField('photo', upload_to='media/actors_photo', null=True, blank=True)

    def __str__(self):
        return self.last_name


class Access(models.Model):
    type_of_access = models.CharField('type of access', null=True, blank=True, max_length=255)

    def __str__(self):
        return self.type_of_access


class Genre(models.Model):
    name = models.CharField('name of genre', max_length=255, blank=True, null=True)
    films = models.ManyToManyField(Film, related_name='genre_film', blank=True)

    def __str__(self):
        return self.name
