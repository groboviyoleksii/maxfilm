from django.shortcuts import render

# Create your views here.
from django.views.generic import ListView, TemplateView
from django.views.generic.base import View

from main.models import Film, Genre


class IndexView(View):
    def get(self, *args, **kwargs):
        new_films = Film.objects.order_by('-release_date')[:3]
        most_popular = Film.objects.order_by('-fake_views')[:6]
        context = {
            'new_films': new_films,
            'most_popular': most_popular
        }
        return render(self.request, '../templates/main/index.html', context=context)


class FilmsPageView(ListView):
    model = Film
    template_name = '../templates/main/films_page.html'
    context_object_name = 'films'
    paginate_by = 8

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['genres'] = Genre.objects.all()
        return context
