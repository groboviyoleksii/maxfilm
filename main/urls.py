from django.urls import path, include
from MaxFilm import settings
from .views import IndexView, FilmsPageView

urlpatterns = [
    path('', IndexView.as_view(), name='index'),
    path('films-page', FilmsPageView.as_view(), name='films_page')
]
