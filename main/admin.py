from django.contrib import admin

from main.models import Film, Actor, Genre, Access

admin.site.register(Film)
admin.site.register(Actor)
admin.site.register(Genre)
admin.site.register(Access)
