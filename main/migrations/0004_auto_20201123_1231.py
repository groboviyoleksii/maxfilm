# Generated by Django 3.1.3 on 2020-11-23 12:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0003_auto_20201123_1227'),
    ]

    operations = [
        migrations.AlterField(
            model_name='actor',
            name='films',
            field=models.ManyToManyField(null=True, to='main.Film'),
        ),
    ]
